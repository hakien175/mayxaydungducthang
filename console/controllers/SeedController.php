<?php

namespace console\controllers;
use common\models\Users;

class SeedController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $faker = \Faker\Factory::create();

        $user = new Users();

        $profile = new Profile();
        for ( $i = 1; $i <= 20; $i++ )
        {
            $user->username = $faker->username;
            $user->email = $faker->email;
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword(123456);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            $user->save();
            
        }
    }

}
