<?php

namespace backend\controllers;

use common\models\AuthAssignment;
use Yii;
use common\models\InfoProfile;
use common\models\query\InfoProfileQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InfoProfileController implements the CRUD actions for InfoProfile model.
 */
class InfoProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all InfoProfile models.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $auths = AuthAssignment::find()->where(['user_id'=>$id])->all();

        $model = $this->findModel($id);$str = $model->avatar;
        if ($model->load(Yii::$app->request->post())) {
            $model->birth_day = strtotime($model->birth_day);
            if ($model->avatar == $str){
                $model->avatar = $str;
            }else{
                $data = $model->avatar;
                if(!empty($model->avatar)){
                    $fileName = explode('+', $data);
                    $strFileName = "$fileName[0]+";
                    
                    $position = strpos($strFileName,'.',0);
                    $strFileName = substr($strFileName, 0, $position).strtotime("now").'.jpg';
         
                    $strEx = explode('base64,',$data);
                    $imgFile = base64_decode($strEx[1]);
        
                    file_put_contents(Yii::$app->basePath.'/web/upload/info-profile/'.$strFileName, $imgFile);
                    $model->avatar = $strFileName;
                }
            }
           
            if ($model->save()){
                Yii::$app->session->setFlash('success','Cập nhật thông tin thành công');
                return $this->redirect(['update', 'id' => $id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'auths' => $auths,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = InfoProfile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}